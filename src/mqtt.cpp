#include "secrets.h"
#include <mqtt/helper.h>

MQTT_Service mqtt_service; // reconcile with the superlight mqtt_service

MQTT_PUBLISH_FEED(G_RANGEFINDER, "g-rangefinder");
//MQTT_PUBLISH_FEED(G_DOOR_AUDIT, "g-door-audit");
MQTT_PUBLISH_FEED(G_UPTIME, "g-uptime");
MQTT_SUBSCRIBE_FEED(G_DOOR, "g-door");

// doing this explicitly because we want QoS 2 for this one since it's
// a diagnostic/audit
const char MQTT_FEED_G_DOOR_AUDIT[] PROGMEM = AIO_PREFIX "/feeds/g-door-audit";
Adafruit_MQTT_Publish G_DOOR_AUDIT(&mqtt, MQTT_FEED_G_DOOR_AUDIT, 1);


void setup_mqtt_feeds()
{
  mqtt.subscribe(&G_DOOR);
}


#include "log.h"

bool lastLoggedStateOnline = false;

void log_mqtt_online()
{
  if(mqtt_service.isInitialized() && !lastLoggedStateOnline)
  {
    Logger.verbose("MQTT online");
    lastLoggedStateOnline = true;
  }
  else if(!mqtt_service.isInitialized() && lastLoggedStateOnline)
  {
    Logger.verbose("MQTT offline");
    lastLoggedStateOnline = false;
  }
}
