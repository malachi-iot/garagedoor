#include <Arduino.h>

#include "secrets.h"
#include "menu.h"
#include "mqtt.h"
#include "profile.h"
#include "profile.wifi.h"
#include "log.h"
#include "telnet.h"
#include "ntp.h"
#include <Service.h>
#include <mdns.h>

#include <wifi/helper.h>
#include <fact/debug.h>

#include <TimeLib.h>

extern "C" void esp_yield();

void setup_mqtt_feeds();
void setup_mDNS();
void setup_rangefinder();
void setup_led();

void pause_mDNS();
void resume_mDNS();

extern bool mDNS_paused;

void setup_ntp();

extern NTPState ntpState;

void loop_rangefinder();
void loop_led();
void loop_ntp();
void loop_mDNS();

void log_mqtt_online();

WiFi_Service wifi_service;

#define PIN_DOOR 15
#define PIN_LED 2

void svc_status(util::Service* s)
{
  Serial << s->getName() << F(" svc: ") << s->getStatusMessage();
  Serial.println();
}

uint32_t lastMqttConnectionAttempt = 0;

void mqtt_statusupdate(util::Service* s)
{
  lastMqttConnectionAttempt = millis();
  log_mqtt_online();
}

extern mdns::MDns my_mdns;


void setup()
{
  Serial.begin(115200);

  //delay(3000);
  Serial.println("Mounting FS...");

  if (!SPIFFS.begin()) {
    Serial.println("Failed to mount file system");
    return;
  }

  pinMode(PIN_DOOR, OUTPUT);
  pinMode(PIN_LED, OUTPUT);

  digitalWrite(PIN_LED, HIGH);

  setup_led();

  wifi_service.statusUpdated += svc_status;
  /*
   * we'll need getState to be public for this to work
  wifi_service.statusUpdated += [](util::Service* svc) 
  {
    if(svc->getState() == Started)
    {
      cout << F("IP address: ");
      cout.println(WiFi.localIP());
    }
  };
  */
  mqtt_service.statusUpdated += svc_status;
  mqtt_service.statusUpdated += mqtt_statusupdate;

  //wifiProfileManager.load();
  // activate "ambient" (i.e. hardcoded) wifi profile 1st
  if(!wifiProfileActivator.activate())
    wifiProfileManager.activate();
    
  if(wifi_service.isInitialized())
  {
    cout << F("IP address: ");
    cout.println(WiFi.localIP());
  }

  menu.begin();
  setup_mqtt_feeds();

  // will attempt to activate "ambient" profile sitting in
  // mmqtProfile
  mqttProfileActivator.activate();
  
  setup_mDNS();
  setup_rangefinder();
  setup_ntp();
  setup_telnet();
}

extern Adafruit_MQTT_Subscribe G_DOOR;
extern Adafruit_MQTT_Publish G_RANGEFINDER;
extern Adafruit_MQTT_Publish G_DOOR_AUDIT;
extern Adafruit_MQTT_Publish G_UPTIME;

bool aggressive_wifi_search = false;

void loop()
{
  // lastSend = last time we sent uptime notification
  static uint32_t lastSend = 0;

#ifdef DEBUG2
  if(lastSend == 0) cout << F("Phase 0\r\n");
#endif

  // always do mDNS checks; even though we don't need to for our
  // business logic, it seems that ESP8266 IP stack gets backed up
  // if we don't empty out incoming/subscribed UDP - causing MMQT
  // ping & connect to fail
  loop_mDNS();

  // FIX: sprinkling these around in hopes that ESP8266 will be happier and
  // lose packets less. Mostly just an experiment
  delay(5);
  //esp_yield();

#ifdef DEBUG2
  if(lastSend == 0)
    cout << F("Phase 1\r\n");
#endif

  if(millis() > (lastSend + 3000))
  {
    if(!wifi_service.isInitialized())
    {
      if(aggressive_wifi_search)
      {
        wifiProfileManager.activate();
      }
    }
    // only attempt MQTT activities if WiFi is initialized
    else
    {
      if(!mqtt_service.isInitialized())
      {
        // if we can't connect to MQTT, re-enable mDNS service if it's paused
        // just incase mDNS can provide us with a better MQTT IP address
        if(mDNS_paused)
          resume_mDNS();

        // TODO: only try to connect here if it's been over X amount
        // of time with no connection on EEPROM address, EXCEPT right
        // at startup.  In other words, give EEPROM connection a chance
        // before possibly-rogue mDNS connection tries to take over.
        // needs configuration parameters to fine tune this
        // -- this is partially done now via the else if (millis())

        // try autodetected address, if it exists.  If we've already tried it
        // the fall through (possibly) to profile manager.  This address != server
        // code introduces a glitch though where if we disconnect accidentally
        // from our mDNS-discovered server AND there are no MQTT profiles then
        // we won't reconnect
        
        // all this should get cleared up once we get an aggregate profile manager
        // running
        if(mDNSHost.address != "" && mDNSHost.address != mqttProfile.server)
        {
#ifdef DEBUG
          cout << F("Attempting to activate mDNS-discovered MQTT");
          cout.println();
#endif
          mDNSHost.address.toCharArray(mqttProfile.server, 64);
          // mqttActivator is hardwired to mqttProfile
          mqttProfileActivator.activate();
        }
        else if(millis() - lastMqttConnectionAttempt > 30000)
        {
          // just in case we have no profiles at all, in that situation
          // no event gets fired to update lastMqttConnectionAttempt
          lastMqttConnectionAttempt = millis();

#ifdef DEBUG
          cout << F("Attempting to activate profile MQTT.  Ambient profile server = ");
          cout.println(mqttProfile.server);
#endif

          // FIX: kludge to make sure mDNS gets a new crack at reconnection
          mqttProfile.server[0] = 0;
          
          mqttProfileManager.activate();
        }
      }
      else
      {
        // if we have active and happy MQTT + WiFi, then we don't need to keep
        // listening for more mDNS MQTT messages
        if(!mDNS_paused)
          pause_mDNS();
          
        // don't really need this to keep connection alive per se,
        // but handy incase connection to server is interrupted
        //
        // NOTE: For some reason, I think keepAlive ping dies if
        // UDP traffic has activity at the same time
        if(millis() - lastMqttConnectionAttempt > 30000)
          mqtt_service.keepAlive();

        //G_RANGEFINDER.publish(lastSend);
        lastSend = millis();
        G_UPTIME.publish(lastSend);
    #ifdef DEBUG2
        cout.println("Sending uptime info");
    #endif
      }
    }
  }

  // FIX: sprinkling these around in hopes that ESP8266 will be happier and
  // lose packets less. Mostly just an experiment
  delay(5);
  yield();

#ifdef DEBUG2
  if(lastSend == 0)
    cout << F("Phase 2\r\n");
#endif


  // TODO: look into fancy callbacks instead of polling here,
  // but for now polling is fine
  Adafruit_MQTT_Subscribe *subscription;
  while ((subscription = mqtt.readSubscription()))
  {
    cout << F("Sub received: ");

    if(subscription == &G_DOOR)
    {
      cout << F("Door ");

      int lastread = atoi((char*)subscription->lastread);

      // has to be between 1 and 100 to be a valid identifier
      if(lastread > 0 && lastread <= 100)
      {
        cout << F("open");
        digitalWrite(PIN_DOOR, HIGH);
        digitalWrite(PIN_LED, LOW);
        // FIX: experimenting with sprinkling explicit delays/esp_yields
        //delay(500);
        delay(250);
        //esp_yield();
        delay(250);
        //esp_yield();
        digitalWrite(PIN_DOOR, LOW);
        digitalWrite(PIN_LED, HIGH);
      }
#ifdef DEBUG
      else
      {
        cout << F("unrecognized command: ") << (char*)subscription->lastread;
      }
#endif

      // TODO: add day of week to this also
      char buf[32];
      sprintf(buf, "%d/%d - %d:%02d:%02d: ID = %d",
        month(), day(),
        hourFormat12(), minute(), second(), lastread);

      G_DOOR_AUDIT.publish(buf);

      sprintf(buf, "Open request: %d", lastread);
      Logger.info(buf);
      //delay(500);

      cout.println();
    }
#ifdef DEBUG
    else
    {
      cout << F("Unknown: ") << (PGM_P)(subscription->topic);
      cout.println();
    }
#endif
  }

  // FIX: sprinkling these around in hopes that ESP8266 will be happier and
  // lose packets less. Mostly just an experiment
  delay(5);
  yield();
  //esp_yield();

  console.handler();

  // TODO: only do these UDP related things when WiFi is online
  // and if WiFi bounces, restart the UDP stuff also

  // NTP looping doesn't interfere with mDNS code, so run it always
  loop_ntp();

#ifdef DEBUG2
  if(lastSend == 0)
  {
    cout << F("Phase 3\r\n");
    lastSend++;
  }
#endif

  loop_telnet();

  // strangely, this rangefinder code which appears to screw up UDP
  // and networking behavior.
  // wonder if its related to this: https://github.com/esp8266/Arduino/issues/513
  loop_rangefinder();

  yield();
}
