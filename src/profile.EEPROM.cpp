#include "profile.h"

#include <EEPROM.h>

template <WiFiProfile& profile>
class WiFiEEPROMProfileFormatter : public IProfileFormatter<uint16_t>
{
  struct Header
  {
    // active flag primarily means does it exist in eeprom, but perhaps can
    // also mean for multiple profiles which is active
    bool isActive() { return !_isActive; }
    bool setActive() { _isActive = 0; }

  private:
    uint8_t _isActive : 1;

  public:
    uint8_t version : 7;
  };
  
public:
  virtual bool load(uint16_t in, const size_t size)
  {
    union
    {
      Header header;
      uint8_t header_byte;
    };
    
    header_byte = EEPROM.read(in);
    EEPROM.get(in + 1, profile);
  }
  
  virtual bool save(uint16_t out)
  {
    union
    {
      Header header;
      uint8_t header_byte;
    };

    EEPROM.write(out, header_byte);
    EEPROM.put(out + 1, profile);
  }
};

bool EEPROMProfileManager::activate()
{
  return false;
}
