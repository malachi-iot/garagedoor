#include <ESP8266WiFi.h>
#include <mdns.h>
#include <Console.h>
#include <fact/debug.h>

#include "mqtt.h"

// using code/libs from here https://github.com/mrdunk/esp8266_mdns

#define QUESTION_SERVICE "_mqtt._tcp.local"

void answerCallback(const mdns::Answer* answer);

mdns::MDns my_mdns(NULL, NULL, answerCallback);

Host mDNSHost;

namespace mdns
{
  // FIX: hacks into mDNS so that we can manually turn on/off multicast listener
  // since it seems to interfere with our other activities
  extern WiFiUDP Udp;
}

uint32_t lastMDNSresponse;

// When an mDNS packet gets parsed this callback gets called once per Query.
// See mdns.h for definition of mdns::Query.
void answerCallback(const mdns::Answer* answer)
{
#ifdef DEBUG3
  cout << '~';
#endif
  
  if (answer->rrtype == MDNS_TYPE_PTR and strstr(answer->name_buffer, QUESTION_SERVICE) != 0)
  {
    mDNSHost.serviceName = answer->rdata_buffer;
#ifdef DEBUG2
    cout << F("Service found: ") << answer->rdata_buffer;
    cout.println();
#endif
  }

  // A typical SRV record matches a human readable name to port and FQDN info.
  // eg:
  //  name:    Mosquitto MQTT server on twinkle.local
  //  data:    p=0;w=0;port=1883;host=twinkle.local
  if (answer->rrtype == MDNS_TYPE_SRV)
  {
    unsigned int i = 0;
    {
      if (mDNSHost.serviceName == answer->name_buffer)
      {
        // This hosts entry matches the name of the host we are looking for
        // so parse data for port and hostname.
        char* port_start = strstr(answer->rdata_buffer, "port=");
        if (port_start)
        {
          port_start += 5;
          char* port_end = strchr(port_start, ';');
          char port[1 + port_end - port_start];
          strncpy(port, port_start, port_end - port_start);
          port[port_end - port_start] = '\0';

          if (port_end)
          {
            char* host_start = strstr(port_end, "host=");
            if (host_start)
            {
              host_start += 5;
              mDNSHost.port = port;
              mDNSHost.name = host_start;
            }
          }
        }
      }
    }
  }

  // A typical A record matches an FQDN to network ipv4 address.
  // eg:
  //   name:    twinkle.local
  //   address: 192.168.192.9
  if (answer->rrtype == MDNS_TYPE_A)
  {
    if (mDNSHost.name == answer->name_buffer)
      mDNSHost.address = answer->rdata_buffer;
  }

  if(mDNSHost.serviceName != "" && mDNSHost.port != "" && mDNSHost.address != "")
  {
    // only register mDNS response if we get a serviceable collection of data
    lastMDNSresponse = millis();

#ifdef DEBUG
    cout.print(F("> "));
    cout.print(mDNSHost.name);
    cout.print("  ");
    cout.print(mDNSHost.serviceName);
    cout.print("  ");
    cout.print(mDNSHost.address);
    cout.print(':');
    cout.print(mDNSHost.port);
    cout.println();
    // my << code isn't working for proper strings yet
    //cout << F("> ") << mDNSHost.serviceName << ':' mDNSHost.port << ' ';
    //cout << mDNSHost.name << ' ' << mDNSHost.address;
    //cout.println();
#endif
  }
}

void setup_mDNS()
{
  // Query for all host information for a paticular service. ("_mqtt" in this case.)
  my_mdns.Clear();
  struct mdns::Query query_mqtt;
  strncpy(query_mqtt.qname_buffer, QUESTION_SERVICE, MAX_MDNS_NAME_LEN);
  query_mqtt.qtype = MDNS_TYPE_PTR;
  query_mqtt.qclass = 1;    // "INternet"
  query_mqtt.unicast_response = 0;
  my_mdns.AddQuery(query_mqtt);
  my_mdns.Send();
}

bool mDNS_paused = false;


void pause_mDNS()
{
#ifdef DEBUG
  cout.println("Pausing mDNS");
#endif
  mdns::Udp.stop();
  mDNS_paused = true;
}


void resume_mDNS()
{
#ifdef DEBUG
  cout.println("Resuming mDNS");
#endif
  setup_mDNS();
  mdns::Udp.beginMulticast(WiFi.localIP(), IPAddress(224, 0, 0, 251), MDNS_TARGET_PORT);
  mDNS_paused = false;
}

void loop_mDNS()
{
  // if we're not even checking mDNS, don't do mDNS housekeeping
  if(mDNS_paused) return;
  
  // if 1 minute goes by without an mDNS response, try restarting
  // multicast listener (sometimes it seems to need this)
  if(millis() - lastMDNSresponse > 60000)
  {
    pause_mDNS();
    cout.println(F("Restarting multicast"));
    resume_mDNS();
    
    // just to shut it up for another 2m, create a fake response registration
    lastMDNSresponse = millis();
  }
  
  my_mdns.Check();
}
