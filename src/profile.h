#pragma once

#include <fact/Menu.h>
#include <fact/event.h>
#include <FS.h>

struct WiFiProfile
{
  char ssid[32];
  char pass[32];
};

struct MQTTProfile
{
  char server[64];
  char password[64];
};


extern bool aggressive_wifi_search;

// Profile formatter can save or load a profile (instance variable of formatter)
// to/from a data store
// it also holds on to the profile data itself in a singleton-like way
template <class TRaw>
class IProfileFormatter
{
public:
  virtual bool load(TRaw in, const size_t size = 0) = 0;
  virtual bool save(TRaw out) = 0;
};

class IProfileActivator
{
public:
  virtual bool activate() = 0;
};



template <class TRaw>
class IProfileManager : public IProfileActivator
{
protected:
  IProfileFormatter<TRaw>& formatter;
  IProfileActivator& activator;

public:
  // When we merge in service branch we can use this
  //events::Event<const char*> profileFound;

  IProfileManager(IProfileFormatter<TRaw>& _formatter, IProfileActivator& _activator) :
    formatter(_formatter),
    activator(_activator)
  {}
};

template <class IProfileActivator& ... Activators>
class AggregateProfileActivator : public IProfileActivator
{
  
};

template <class IProfileActivator& profileActivator, class IProfileActivator& ... Activators>
class AggregateProfileActivator<profileActivator, Activators...> : 
  public AggregateProfileActivator<Activators...>
{
public:
  void activate() {}  
};




class SPIFFSProfileManager : public IProfileManager<Stream&>
{
  PGM_P directory;

  bool load(File& profileFile);
  bool activate(File& profileFile);

public:
  SPIFFSProfileManager(
    IProfileFormatter<Stream&>& profileFormatter,
    IProfileActivator& _activator, PGM_P directory) :
    IProfileManager<Stream&>(profileFormatter, _activator),
    directory(directory)
    {}

  virtual bool activate() override;

  // saves current profile to filename
  // current = what's in memory
  // activated = what the underlying subsystem (WiFi, MMQT, etc) is actually
  //             using.  We expect the two to align, but they don't always
  void save(const char* filename = "default");

  // loads profile from filename into current profile area
  // doesn't activate it
  bool load(const char* filename = "default");

  bool activate(uint8_t retries, uint16_t timeout)
  {
    while(retries--)
    {
      if(activate()) return true;

      cout << "Retries left: " << retries;
      cout.println();

      delay(timeout);
    }

    return false;
  }
};


class EEPROMProfileManager : public IProfileManager<uint16_t>
{
public:
  EEPROMProfileManager(
    IProfileFormatter<uint16_t>& _formatter,
    IProfileActivator& _activator) :
    IProfileManager<uint16_t>(_formatter, _activator)
    {}

  virtual bool activate() override;
};


extern WiFiProfile wifiProfile;
extern MQTTProfile mqttProfile;
extern SPIFFSProfileManager wifiProfileManager;
extern SPIFFSProfileManager mqttProfileManager;
