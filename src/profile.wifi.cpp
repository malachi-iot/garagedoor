#include "secrets.h"
#include "profile.h"
#include <wifi/service.h>

#include "main.h" // for wifi_service itself

#include "profile.wifi.h"

#include <ArduinoJson.h>

template <WiFiProfile& profile>
class WiFiJSONProfileFormatter : public IProfileFormatter<Stream&>
{
public:
  virtual bool load(Stream& in, const size_t size);
  virtual bool save(Stream& out);
};


template <WiFiProfile& profile>
bool WiFiJSONProfileFormatter<profile>::load(Stream& in, const size_t size)
{
  bool result = false;

  char* buffer = new char[size];

  in.readBytes(buffer, size);
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& json = jsonBuffer.parseObject(buffer);

  if(json.success())
  {
    // TODO: eventually make this work without ssid-pass present
    if(json.containsKey("ssid") && json.containsKey("ssid-pass"))
    {
      strcpy(profile.ssid, json["ssid"]);
      strcpy(profile.pass, json["ssid-pass"]);

  #ifdef DEBUG
      // doesn't like living in a template , cuz of F() expansion
      //cout << F("ssid: ")  << profile.ssid;
      //cout.println();
      //cout << F("pass: ") << profile.pass;
  #endif
      result = true;
    }
  }

  delete buffer;
  return result;
}

template <WiFiProfile& profile>
bool WiFiJSONProfileFormatter<profile>::save(Stream& out)
{
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& json = jsonBuffer.createObject();

  json["ssid"] = profile.ssid;
  json["ssid-pass"] = profile.pass;

  json.printTo(out);
}


// ESP8266 has interesting feature where when SSID/PASS are blank, it uses
// an EEPROM-cached last-known-good connection
//WiFiProfile wifiProfile; 

WiFiProfile wifiProfile = { WLAN_SSID, WLAN_PASS };
WiFiJSONProfileFormatter<wifiProfile> wifiProfileFormatter;
WiFiProfileActivator<wifiProfile> wifiProfileActivator;
SPIFFSProfileManager wifiProfileManager(wifiProfileFormatter, wifiProfileActivator, "/etc/network/wifi/");
