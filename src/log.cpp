#include "log.h"

#include <fact/lib.h>

void showDate(Stream& cout);
void showTime(Stream& cout);

namespace _log
{
  SPIFFSAppender Logger::appender;

  void SPIFFSAppender::write(LogLevel level, const char* str)
  {
    // FIX: Not very performant, but I don't yet know if we can
    // keep file handles open for a long time
    File logFile = SPIFFS.open("/var/log/syslog", "a");

    // FIX: Need to keep file open to really do datetime output properly
    logFile.print(str);
    logFile.close();
  }

  void SPIFFSAppender::log(LogLevel level, const char* str)
  {
    // FIX: Not very performant, but I don't yet know if we can
    // keep file handles open for a long time
    File logFile = SPIFFS.open("/var/log/syslog", "a");

    // FIX: Need to keep file open to really do datetime output properly
    showDate(logFile);
    logFile.print(' ');
    showTime(logFile);

    char levelChar;
    
    switch(level)
    {
      case Info:    levelChar = 'I'; break;
      case Warning: levelChar = 'W'; break;
      case Error:   levelChar = 'E'; break;
      case Verbose: levelChar = 'V'; break;
      default:      levelChar = '?'; break;
    }
    logFile << '|' << levelChar << '|';
    logFile.println(str);
    logFile.close();
  }

  void Logger::log(LogLevel level, const char* str)
  {
    appender.log(level, str);
    //appender.write(level, str);
    //appender.write(level, "\r\n");
  }
}

_log::Logger Logger;
