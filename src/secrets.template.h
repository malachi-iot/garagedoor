#define WLAN_SSID       "your-wifi-ssid"
#define WLAN_PASS       "your-wifi-pass"

#define AIO_SERVER      "your MQTT server address"
#define AIO_SERVERPORT  1883
#define AIO_USERNAME    ""
#define AIO_KEY         "your MQTT unique auth key"
// AIO_PREFIX  can be left undefined, in which case it automatically is
//  assigned to AIO_USERNAME
#define AIO_PREFIX      "your MQTT feed prefix"
