#pragma once

enum NTPState
{
  // ask for time
  SendPacket,
  // wait for response
  SendWait,
  // receive the time
  ReceivePacket,
  // time to wait before asking for time again
  ReceiveWait
};
