#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <Console.h>
#include <taskmanager.h>

#include <TimeLib.h>

#include "log.h"
#include "ntp.h"

using namespace util;

// lifted from here https://github.com/esp8266/Arduino/blob/master/libraries/ESP8266WiFi/examples/NTPClient/NTPClient.ino
// interesting code here also https://github.com/PaulStoffregen/Time/blob/master/examples/TimeNTP/TimeNTP.ino

IPAddress timeServerIP; // time.nist.gov NTP server address
const char* ntpServerName = "time.nist.gov";

// FIX: see if there's a more graceful way to utilize UDP. Does making a 2nd one burn a lot
// of code space?  Does it coexist with MDns' one?
//extern WiFiUDP MDns::Udp;

// i think as long as distinct ports are used, separate WiFiUDP won't collide
// but not sure~
WiFiUDP udp;

const uint16_t ntp_port = 2390;
const int NTP_PACKET_SIZE = 48; // NTP time stamp is in the first 48 bytes of the message

// how many milliseconds to wait until requesting time again
const uint32_t NTP_TIMEOUT = 60000 *
#ifdef DEBUG
5;
#else
30;
#endif

//const int timeZone = 1;     // Central European Time
//const int timeZone = -5;  // Eastern Standard Time (USA)
//const int timeZone = -4;  // Eastern Daylight Time (USA)
//const int timeZone = -8;  // Pacific Standard Time (USA)
const int timeZone = -7;  // Pacific Daylight Time (USA)

byte packetBuffer[ NTP_PACKET_SIZE]; //buffer to hold incoming and outgoing packets

// send an NTP request to the time server at the given address
unsigned long sendNTPpacket(const IPAddress& address)
{
#ifdef DEBUG
  cout.println("sending NTP packet...");
#endif
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12]  = 49;
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;

  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  udp.beginPacket(address, 123); //NTP requests are to port 123
  udp.write(packetBuffer, NTP_PACKET_SIZE);
  udp.endPacket();
}

ScheduleManager scheduleManager;

NTPState ntpState;
uint32_t ntpWakeup;

class Wakeup : IScheduledWithInterval<uint16_t>
{
public:
  void execute() override
  {
    switch(ntpState)
    {
      case SendWait: ntpState = ReceivePacket;
      case ReceiveWait: ntpState = SendPacket;
    }
  }
};


Wakeup ntpWakeup2;

void setup_ntp()
{
  udp.begin(ntp_port);
}


void loop_ntp()
{
  // TODO: eventually turn this into a service and take the timeFound/time assignment
  // code out and make them event responders
  static bool timeFound = false;
  
  if(ntpState == SendPacket)
  {
    WiFi.hostByName(ntpServerName, timeServerIP);

    sendNTPpacket(timeServerIP);
    //scheduleManager.add(ntpWakeup2);
    // ask every 10s until we get a response
    ntpWakeup = millis() + 10000;
    ntpState = SendWait;
    return;
  }
  else if(ntpState == SendWait)
  {
    //if(millis() > ntpWakeup) ntpState = ReceivePacket;
    int cb = udp.parsePacket();
    if (!cb)
    {
      // if we still have no response and it's past timeout,
      // initiate request again
      if(millis() > ntpWakeup)
      {
#ifdef DEBUG
        cout.println("no NTP packet yet");
#endif
        ntpState = SendPacket;
       // ask every 10s until we get a response
       //ntpWakeup = millis() + 10000;
      }
    }
    else
    {
      cout.print("NTP packet received, length=");
      cout.println(cb);

#ifdef FACT_LIB_STRICT
      if(cb != NTP_PACKET_SIZE)
      {
        cout << F("Invalid packet length.  Expected ") << NTP_PACKET_SIZE;
        cout.println();
      }
#endif

      ntpState = ReceivePacket;
    }
    return;
  }
  //delay(1000);
  else if(ntpState == ReceivePacket)
  {
     // We've received a packet, read the data from it
     udp.read(packetBuffer, NTP_PACKET_SIZE); // read the packet into the buffer

     //the timestamp starts at byte 40 of the received packet and is four bytes,
     // or two words, long. First, esxtract the two words:

     unsigned long highWord = word(packetBuffer[40], packetBuffer[41]);
     unsigned long lowWord = word(packetBuffer[42], packetBuffer[43]);
     // combine the four bytes (two words) into a long integer
     // this is NTP time (seconds since Jan 1 1900):
     unsigned long secsSince1900 = highWord << 16 | lowWord;

#ifdef DEBUG
     cout.print("Seconds since Jan 1 1900 = " );
     cout.println(secsSince1900);
#endif
     // Unix time starts on Jan 1 1970. In seconds, that's 2208988800:
     const unsigned long seventyYears = 2208988800UL;
     // subtract seventy years:
     unsigned long epoch = secsSince1900 - seventyYears;

#ifdef DEBUG
     // now convert NTP time into everyday time:
     cout.print("Unix time = ");
     // print Unix time:
     cout.println(epoch);

     // print the hour, minute and second:
     cout.print("The UTC time is ");       // UTC is the time at Greenwich Meridian (GMT)
     cout.print((epoch  % 86400L) / 3600); // print the hour (86400 equals secs per day)
     cout.print(':');
     if ( ((epoch % 3600) / 60) < 10 ) {
       // In the first 10 minutes of each hour, we'll want a leading '0'
       cout.print('0');
     }
     cout.print((epoch  % 3600) / 60); // print the minute (3600 equals secs per minute)
     cout.print(':');
     if ( (epoch % 60) < 10 ) {
       // In the first 10 seconds of each minute, we'll want a leading '0'
       cout.print('0');
     }
     cout.println(epoch % 60); // print the second
#endif

    // brute force synchronization, adjusting for hardcoded timezone
    setTime(epoch + timeZone * SECS_PER_HOUR);

    // wait NTP_TIMEOUT before asking for the time again, since we got a successful time
    ntpWakeup = millis() + NTP_TIMEOUT;
    
    if(!timeFound)
    {
      timeFound = true;
      char buffer[64];
      sprintf(buffer, "System online: %d seconds", millis() / 1000);
      Logger.info(buffer);
    }

    ntpState = ReceiveWait;
  }
  else if(ntpState == ReceiveWait)
  {
  // wait ten seconds before asking for the time again
  //delay(10000);
    if(millis() > ntpWakeup) ntpState = SendPacket;
  }
}
