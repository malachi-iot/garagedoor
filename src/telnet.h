#pragma once

#include "features.h"

#ifdef FEATURE_TELNET
void setup_telnet();
void loop_telnet();
#else
void setup_telnet() {}
void loop_telnet() {}
#endif
