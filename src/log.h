#pragma once

// looked but didn't find any ESP8266 logging facilities.
// easyloggingpp looked promising but I don't think it handles
// "custom appenders" so it would take some reworking to work with SPIFFS
#include <FS.h>

namespace _log
{
  enum LogLevel : uint8_t
  {
    Verbose,
    Debug,
    Info,
    Warning,
    Error
  };

  class LogStream : Stream
  {
    LogLevel level;
  public:
    LogStream(LogLevel level) : level(level) {}
  };

  // log4net style appender
  class IAppender
  {
  public:
    virtual void write(LogLevel level, const char* str) = 0;
  };

  // TODO: rolling file so on day boundaries
  class SPIFFSAppender : IAppender
  {
  public:
    virtual void write(LogLevel level, const char* str) override;

    void log(LogLevel level, const char* str);

    class Context
    {
      File& file;
    public:
      Context(File& file) : file(file) {}
      Context(Context& context) : file(context.file) {}

      void write(const char* str) { file.print(str); }
    };
  };

  class Logger
  {
    static SPIFFSAppender appender;

  public:
    static void log(LogLevel level, const char* str);
    
    static void verbose(const char* str)
    {
      log(Verbose, str);
    }

    static void info(const char* str)
    {
      log(Info, str);
    }

    static void warn(const char* str)
    {
      log(Warning, str);
    }

    static void error(const char* str)
    {
      log(Error, str);
    }
  };
}

extern _log::Logger Logger;
