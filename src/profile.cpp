#include <Arduino.h>
#include <EEPROM.h>

#include "profile.h"
#include <wifi/service.h>

// spiffs
#include <FS.h>
// to interpret config files
#include <ArduinoJson.h>


using namespace util;

extern WiFi_Service wifi_service;
