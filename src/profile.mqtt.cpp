#include "profile.h"
#include <mqtt/service.h>
#include "mqtt.h"
#include "secrets.h"

#include <ArduinoJson.h>

using namespace util;


template <MQTTProfile& profile>
class MQTTJSONProfileFormatter : public IProfileFormatter<Stream&>
{
public:
  virtual bool load(Stream& in, const size_t size);
  virtual bool save(Stream& out);
};


template <MQTTProfile& profile>
bool MQTTJSONProfileFormatter<profile>::load(Stream& in, const size_t size)
{
  bool result = false;

  char* buffer = new char[size];

  in.readBytes(buffer, size);
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& json = jsonBuffer.parseObject(buffer);

  if(json.success())
  {
    if(json.containsKey("server"))
    {
      strcpy(profile.server, json["server"]);
      if(json.containsKey("password"))
        strcpy(profile.password, json["password"]);

      result = true;
    }
  }

  delete buffer;
  return result;
}

template <MQTTProfile& profile>
bool MQTTJSONProfileFormatter<profile>::save(Stream& out)
{
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& json = jsonBuffer.createObject();

  json["server"] = profile.server;
  json["password"] = profile.password;

  json.printTo(out);
  return false;
}


MQTTProfile mqttProfile = { AIO_SERVER };
MQTTJSONProfileFormatter<mqttProfile> MQTTFormatter;
MQTTProfileActivator<mqttProfile> mqttProfileActivator;
SPIFFSProfileManager mqttProfileManager(MQTTFormatter, mqttProfileActivator, "/etc/service/MQTT/");
