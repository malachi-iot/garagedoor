#pragma once

#include <mqtt/service.h>

#include "profile.h"

struct Host
{
  // Experimenting, I don't normally use "String" due to memory constraints
  // but example code does -- so let's try it
  String name;
  String port;
  String address;
  String serviceName;
};



extern MQTT_Service mqtt_service;

template <const MQTTProfile& profile>
class MQTTProfileActivator : public IProfileActivator
{
public:
  virtual bool activate() override
  {
    mqtt.setServerName(profile.server);
    mqtt_service.restart();
    return mqtt_service.isInitialized();
  }
};

extern Host mDNSHost;


// UNTESTED
// DO NOT USE.  We should do this thru a faux profile manager instead (although
// it will look super similar to this)
class MQTTmDNSProfileActivator : MQTTProfileActivator<mqttProfile>
{
public:
  virtual bool activate() override
  {
    mDNSHost.address.toCharArray(mqttProfile.server, sizeof(mqttProfile.server));
    return MQTTProfileActivator<mqttProfile>::activate();
  }
};

extern MQTTProfileActivator<mqttProfile> mqttProfileActivator;
extern MQTTmDNSProfileActivator mqttMDNSProfileActivator;
