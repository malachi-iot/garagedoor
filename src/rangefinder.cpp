#include <SharpIR.h>

#define ir A0
#define model 20150
// ir: the pin where your sensor is attached
// model: an int that determines your sensor:  1080 for GP2Y0A21Y
//                                            20150 for GP2Y0A02Y
//                                            (working distance range according to the datasheets)

SharpIR SharpIR(ir, model);

int rangefinder_distance;

void setup_rangefinder()
{
}

void loop_rangefinder()
{
  // wakeup code relates to this  https://github.com/esp8266/Arduino/issues/513
  static uint32_t nextWakeup;
  
  if(millis() > nextWakeup)
  {
    rangefinder_distance = SharpIR.distance();
    // link says we can check as fast as once every 10ms, but slower than 3s
    // we don't need
    // very quick so wait 500ms
    nextWakeup += 500;
  }
}
