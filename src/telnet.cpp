#include <ESP8266WiFi.h>

#include <Console.h>
#include "telnet.h"

#ifdef FEATURE_TELNET
WiFiServer telnetServer(23);
WiFiClient telnetClient;
// holding off on the rest until I actually test CONSOLE_FEATURE_COUT

void setup_telnet()
{
  telnetServer.begin();
}


void loop_telnet()
{
  if(telnetServer.hasClient())
  {
    cout << F("Telnet client connecting");
    cout.println();

    telnetClient = telnetServer.available();
    if(telnetClient.connected())
    {
      telnetClient.println("You got this from ESP8266!");
    }

    telnetClient.stop();
  }
}

#endif
