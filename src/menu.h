#pragma once

#include <Console.h>
#include <fact/MenuFunction.h>
#include <mqtt/service.h>

namespace garageopener
{
  class MainMenu : public util::Menu
  {
  public:
    void begin();
  };
}


extern garageopener::MainMenu menu;
extern util::ConsoleMenu console;
extern util::MenuService menuMqtt;
