#include "profile.h"

bool SPIFFSProfileManager::load(File& profileFile)
{
  size_t size = profileFile.size();

  return formatter.load(profileFile, size);
}

bool SPIFFSProfileManager::activate(File& profileFile)
{
  // if file is invalid, return false right away
  if(!profileFile) return false;

  // if we're able to load the profile, then try to activate it here
  if(load(profileFile))
    return activator.activate();

  return false;
}

bool SPIFFSProfileManager::activate()
{
  String defaultPath = directory;

  defaultPath += "default";

  // TODO: when a profile is found, optionally set it
  // to the new default
  {
    File profileFile = SPIFFS.open(defaultPath, "r");

#ifdef DEBUG
      cout << F("Loading (default) profile file: ") << defaultPath;
      cout.println();
#endif

    // if file is valid and we can activate it, return true right away
    if(activate(profileFile)) return true;
  }

  {
    Dir dir = SPIFFS.openDir(directory);
    while (dir.next())
    {
      auto fileName = dir.fileName();
      
      // skip default file, since we've already attempted that one
      if(fileName == defaultPath)
        continue;
        
#ifdef DEBUG
      cout << F("Loading profile file: ");
      cout.println(fileName);
#endif
      File f = dir.openFile("r");
      if(activate(f)) return true;
    }
  }
  
  return false;
}


void SPIFFSProfileManager::save(const char* filename)
{
  String path = directory;
  
  path += filename;
  
#ifdef DEBUG
  cout << F("Saving profile file: ") << path;
  cout.println();
#endif

  File f = SPIFFS.open(path, "w");
  
  formatter.save(f);
}

bool SPIFFSProfileManager::load(const char* filename)
{
  String path = directory;

  path += filename;
  
#ifdef DEBUG
  cout << F("Loading profile file: ") << path;
  cout.println();
#endif

  File profileFile = SPIFFS.open(path, "r");
  
  // if file is invalid, return false right away
  if(!profileFile) return false;

  return load(profileFile);
}
