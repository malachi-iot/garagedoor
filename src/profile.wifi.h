#pragma once

#include "profile.h"

extern WiFi_Service wifi_service;

template <const WiFiProfile& profile>
class WiFiProfileActivator : public IProfileActivator
{
public:
  virtual bool activate() override
  {
    // need to actually go through service, because WiFi can take up to 5s
    // to properly connect
    wifi_service.setSSID(profile.ssid, profile.pass);
    wifi_service.restart();
    if(wifi_service.isInitialized())
    {
      // TODO: can't use F() here , need a non-templated function call
      // to do that
      cout << ("Connected to: ") << profile.ssid;
      cout.println();
      return true;
    }
    else 
      return false;
  }
};


extern WiFiProfileActivator<wifiProfile> wifiProfileActivator;
