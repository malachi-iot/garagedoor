#include "menu.h"

#include <wifi/service.h>
#include <mqtt/service.h>
#include "profile.h"
#include "profile.wifi.h" // this has ref to wifi_service

#include "main.h"

#include "mqtt.h"
#include "secrets.h"

#include <TimeLib.h>

// SPIFFS
#include <FS.h>

using namespace util;

garageopener::MainMenu menu;
ConsoleMenu console(&menu);

extern int rangefinder_distance;
extern uint32_t lastMDNSresponse;

void showDate(Stream& cout)
{
  cout << month() << '/' << day();
}

void showTime(Stream& cout)
{
  auto h = hourFormat12();
  if(h < 10) cout << ' ';
  cout << h << ':';
  auto m = minute();
  if(m < 10) cout << '0';
  cout << m << ':';
  auto s = second();
  if(s < 10) cout << '0';
  cout << s;
}

void showStatus(IMenu::Parameters p)
{
  cout << F("IoT Garage Opener v" GO_VERSION) << F("   time = ");
  showTime(cout);
  cout.println();
  cout << F("ClientID = ") << F(AIO_CLIENTID);
  cout.println();
  cout.println();

  cout << "WiFi: ";
  cout.println();
  //Menu::showKeyValuePair(F("SSID"), wifi_service.getSSID(), 10);
  cout << F(" ssid = ") << wifi_service.getSSID();
  cout << (wifi_service.isInitialized() ? F(" in use") : F(" attemping, but no connection"));
  cout.println();
  //Menu::showKeyValuePair(F("pass"), wifi_service.getPASS(), 10);
  cout << F(" pass = ") << wifi_service.getPASS();
  cout.println();
  cout << F(" mqtt = ");
  cout.print(mDNSHost.address);
  cout.println(F(" (mDNS detected)"));

  // for some reason, rendering mmqt.getServerName() fails
  cout << F(" mqtt = ") << mqttProfile.server;

  if(mqtt_service.isInitialized())
    cout << F(" in use");
  else
    cout << F(" attempting, but no connection");

  cout.println();
  cout << F(" mDNS last response = ") << lastMDNSresponse;
  
  cout.println();
  cout << F(" rangefinder = ") << rangefinder_distance;
}


void set_wifi_ssid(const char* ssid)
{
  // wifi_service *always* points to wifiProfile.ssid & pass
  strcpy(wifiProfile.ssid, ssid);
}

void set_wifi_pass(const char* pass)
{
  strcpy(wifiProfile.pass, pass);
}


void set_mqtt_host(const char* host)
{
  // this only works if mDNS isn't active.  Otherwise mDNS will overwrite it
  strcpy(mqttProfile.server, host);
  mqtt.setServerName(mqttProfile.server);
  cout << F("Set MQTT address to ") << host << F(".  Please restart");
  cout.println();
}

void mqtt_autodetect()
{
  cout << F("Setting address to = ");
  cout.println(mDNSHost.address);
  mDNSHost.address.toCharArray(mqttProfile.server, sizeof(mqttProfile.server));
  mqtt.setServerName(mqttProfile.server);
  cout << F("Please restart MQTT service");
  cout.println();
}


void mDNS_clear()
{
  mDNSHost.address = "";
}

void spiffs_format()
{
  SPIFFS.format();
}



#include "log.h"

void log_test()
{
  Logger.info("test");
}


void cat(const char *filename);

void log_clear()
{
  SPIFFS.remove("/var/log/syslog");
}

void loadProfile(const char* name)
{
  wifiProfileManager.load(name);
}

void saveProfile(const char* name)
{
  wifiProfileManager.save(name);
}

// UNTESTED
void saveProfileMQTT()
{
  mqttProfileManager.save();
}


char cwd[64];

void cdCommand(util::IMenu::Parameters p)
{
  if(p.count == 0)
  {
    cout.println(cwd);
  }
  else if(p.count == 1)
  {
    strcpy(cwd, p.parameters[0]);
    auto len = strlen(cwd);
    if(cwd[len - 1] != '/')
    {
      cwd[len] = '/';
      cwd[len + 1] = 0;
    }
    cout << F("Directory is now: ");
    cout.println(cwd);
  }
}



void ls()
{
  Dir dir = SPIFFS.openDir(cwd);
  String _cwd = cwd;
  while (dir.next())
  {
    auto fileName = dir.fileName();

    // helps, but really we need entire filepath minus filename
    // and filter that way
    if(fileName.startsWith(_cwd))
    {
      cout << dir.fileSize() << ' ';
      cout.println(fileName);
    }
  }
}


// make proposed filenames interact with cwd
String manglePath(const char *filename)
{
  String path;

  if(filename[0] == '/')
  {}
  else
    path = cwd;

  path += filename;

  return path;
}

void cat(const char *filename)
{
  String path = manglePath(filename);

  File f = SPIFFS.open(path, "r");

  if(!f)
  {
    cout << F("Can't read file: ") << path;
    cout.println();
  }
  else
  {
    while(f.available())
    {
#ifdef DEBUG2
      cout.print(".");
#endif
      String line = f.readStringUntil('\n');
      Serial.println(line);
    }
  }
}


void cp(const char* source, const char* dest)
{
  String s = manglePath(source);
  if(strcmp(dest, ".") == 0)
    dest = source;

  String d = manglePath(dest);

  File sourceFile = SPIFFS.open(s, "r");
  File destFile = SPIFFS.open(d, "w");

  while(sourceFile.available())
  {
    uint8_t buffer[32];

    const size_t available = sourceFile.read(buffer, 32);
    destFile.write(buffer, available);
  }
}


void rm(const char* file)
{
  SPIFFS.remove(manglePath(file));
}


void mv(const char* source, const char* dest)
{
  SPIFFS.rename(manglePath(source), manglePath(dest));
}


CREATE_MENUFUNCTION(menuDigitalWrite, digitalWrite, "Arduino digital write function");
CREATE_MENUFUNCTION(menuPinMode, pinMode, "Arduino pin mode set function");
CREATE_MENUFUNCTION(menuSetSSID, set_wifi_ssid, "Set SSID memory variable.  Does not restart wifi");
CREATE_MENUFUNCTION(menuSetPass, set_wifi_pass, "Set WiFi password variable.  Does not restart wifi");
CREATE_MENUFUNCTION(menuSetMQTTHost, set_mqtt_host, "Set MQTT host address.  Does not restart MQTT");
CREATE_MENUFUNCTION(menuSetMQTTDetect, mqtt_autodetect, "Attempts to find mqtt host on LAN via mDNS");

CREATE_MENUFUNCTION(menuSPIFFSformat, spiffs_format, "Format the SPIFFS file system");
CREATE_MENUFUNCTION(menuSPIFFSsaveProfile, saveProfile, "Save WiFi profile to the SPIFFS file system");
CREATE_MENUFUNCTION(menuSPIFFSsaveProfileMQTT, saveProfileMQTT, "Save MQTT profile to the SPIFFS file system");
CREATE_MENUFUNCTION(menuSPIFFSloadProfile, loadProfile, "Load WiFi profile from the SPIFFS file system - doesn't activate");

CREATE_MENUFUNCTION(menuLogTest, log_test, "Write to custom SPIFFS logger");

CREATE_MENUFUNCTION(menuMDNS_clear, mDNS_clear, "Reset mDNS-discovered address");

CREATE_MENUFUNCTION(menuLS, ls, "List contents of directory");
CREATE_MENUFUNCTION(menuCat, cat, "Show contents of a file");
CREATE_MENUFUNCTION(menuCP, cp, "Copy a file");
CREATE_MENUFUNCTION(menuRM, rm, "Remove a file");
CREATE_MENUFUNCTION(menuMV, mv, "Move/rename a file");

MenuGeneric menuShowStatus(showStatus);

MenuService menuWiFi(wifi_service);
MenuGeneric menuCDCommand(cdCommand);

namespace garageopener
{
void MainMenu::begin()
{
#ifdef DEBUG3
  cout << F("Assigning menu show status: ") << (uint32_t)(IMenu*)(&menuShowStatus);
  cout.println();
#endif

  add(menuShowStatus, F("status"), F("tbd"));
  add(menuMqtt, F("mqtt"), F("restart mqtt service"));
  add(menuDigitalWrite);
  add(menuPinMode);

  add(menuSetSSID);
  add(menuSetPass);
  add(menuSetMQTTHost);
  add(menuSetMQTTDetect);
  add(menuWiFi, F("wifi"), F("restart wifi service"));
  add(menuSPIFFSformat);

  add(menuSPIFFSsaveProfile);
  add(menuSPIFFSloadProfile);
  add(menuSPIFFSsaveProfileMQTT);
  add(menuLogTest);
  add(menuMDNS_clear);

  add(menuCDCommand, F("cd"), F("Change directory"));
  add(menuLS);
  add(menuCat);
  add(menuCP);
  add(menuRM);
  add(menuMV);
}
}
